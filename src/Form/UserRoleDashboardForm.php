<?php

namespace Drupal\user_role_dashboard\Form;

use Drupal\Core\Form\FormBase;
use Drupal\user\Entity\Role;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Class UserRoleDashboard.
 */
class UserRoleDashboardForm extends FormBase
{

    /**
     * {@inheritdoc}
     */
     public function getFormId()
    {
        return 'rbd_form';
    } 



    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
		$config = $this->config('rbd_form.settings');			
		$roles = Role::loadMultiple();		

		foreach($roles as $rid => $role) {
			
			$role_path = $config->get('path_'.$rid);
			$val_array = $role->toArray();
			$form['dsb_link_' . $rid] = array(
				'#type' => 'textfield',
				'#default_value' => $role_path,
				'#title' => t('Dashboard URL for '. $val_array['label'] . ' role '),
			);
		}
		
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('JOIN'),
            '#prefix' => '<div class="form-submit">',
            '#attributes' => array('class' => array('button', 'button--primary btn btn-bordered form-control')),
            '#suffix' => '</div>'
        );
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
		$config = \Drupal::getContainer()->get('config.factory')->getEditable('rbd_form.settings');
		$roles = Role::loadMultiple();
		foreach($roles as $rid => $role) {	
			 $config->set('path_'.$rid, $form_state->getValue('dsb_link_'.$rid));
		}
		$config->save(); 
        return;
    }
}
