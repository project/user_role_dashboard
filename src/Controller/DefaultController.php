<?php

namespace Drupal\user_role_dashboard\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user_role_dashboard\Form\UserRoleDashboardForm;


/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
   * roleBasedConfig.
   *
   * @return array
   *   The block instance config form.
   */
  public function roleBasedConfig() {
		$form = new UserRoleDashboardForm();
		$data['form'] = \Drupal::formBuilder()->getForm($form);
		$output['user_role_dashboard_form'] = [
		  '#theme' => 'user_role_dashboard_form',
		  '#data' => $data,
		  '#cache' => ['contexts' => ['url.path', 'url.query_args']],
		];
		 return $output;
  }
  
  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
public function access(AccountInterface $account) {   
	return AccessResult::allowed();	
  }
}
