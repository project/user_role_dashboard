CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

User Role Dashboard provides a ability to redirect any user based on role on any custom URL.

The redirects could be configured for individual roles.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

You can follow the [Drupal's](https://www.drupal.org/docs/user_guide/en/extend-module-install.html) steps to download and install a module.


CONFIGURATION
-------------

   * Go to
    * ```Admin > Configuration > Search and metadata > User Role Dasboard```.
	* Here you can set custom URL for each role.



MAINTAINERS
-----------

 * Manuel Garcia - https://www.drupal.org/u/satbirsingh
